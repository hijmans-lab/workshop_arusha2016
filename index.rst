.. Arusha workshop

Spatial Data Analysis and Modeling for Agricultural Development
===============================================================


Welcome to the workshop on "Spatial Data Analysis and Modeling for Agricultural Development, with R".

This is a five day workshop from Monday August 15 to Friday August 19, 2016. The daily program is from 8:30 AM to 5:30 PM.

Instructors: Camila Bonilla, Jiehua Chen, Ani Ghosh, Robert Hijmans, Alex Mandel, and William Wu

Venue
-----

`MS Training Centre for Development Cooperation <http://www.mstcdc.or.tz/>`__, just `outside Arusha <https://www.google.com/maps/place/MS+Training+Centre+for+Development+Cooperation+-+MS+TCDC/@-3.4325014,36.9141818,10.5z/data=!4m5!3m4!1s0x0:0x8375766b5aef097e!8m2!3d-3.37033!4d36.838738>`__, Tanzania. Logding and meals (breakfast, lunch, dinner) are also at MS-TCDC.

Preparation
-----------

Before the course, you need to work through this and take this test using the required :ref:`software-label`.

.. _software-label:

Software
^^^^^^^^

You need to bring a laptop computer (it does not matter whether it is Windows, Mac, or Linux) and install:

* R installed (version 3.3.1); `download it here <http://r.adu.org.za/>`__
* R-Studio desktop (current version 0.99.902 or later); `download it here <https://www.rstudio.com/products/rstudio/download/>`__.

Data
^^^^

You should also bring some of your own data that you would like to work on using R.

Contacts
--------

Contact Lazaro Tango (l.tango@cgiar.org) with questions about itineraries and reimbursements.

Contact Ani Ghosh (anighosh@ucdavis.edu) with questions about the program.



.. toctree::
	:maxdepth: 1
	:hidden:

	day1
	day2
	day3
	day4
	day5
