Friday, August 19
=================

8:30 - Spatial statistics (talk)
--------------------------------
Robert Hijmans `(slides) <_static/talks/day5_1_spatial_5_stats.pdf>`__

9:15 - Projects (lab)
---------------------
Free time to work on your own projects.

++++

10:30 - Coffee
--------------

++++

10:45 - Projects (lab)
----------------------
Free time to work on your own projects.

++++

12:30 - Lunch
-------------

++++

13:30 - Data science tools (talk)
---------------------------------
Alex Mandel `(slides) <_static/talks/day5_2_data-science-tools.pdf>`__


++++

15:00 - Tea
-----------

++++

16:30 - Evaluation
-----------------------------
