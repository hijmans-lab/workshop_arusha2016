Tuesday, August 16
==================


8:30 - Spatial data II (talk)
-----------------------------
Raster data `(slides) <_static/talks/day2_1_spatial_3_raster.pdf>`__


9:00 - Spatial data II (lab)
----------------------------
Raster data manipulation

`handout <_static/labs/day2/day2_lab1.R>`__ and `data <_static/labs/day2/day2_lab1_data.zip>`__

`answers <_static/labs/day2/day2_lab1_answers.R>`__


++++

10:35 - Coffee
--------------

++++

10:45 - Cartography
-------------------
`(slides) <_static/talks/day2_2_spatial_4_maps.pdf>`__


11:00 - Plots and maps with R (lab)
-----------------------------------

General plots `handout <_static/labs/day2/day2_lab2a_graphics.pdf>`__

Maps `handout <_static/labs/day2/day2_lab2b_maps.pdf>`__ , and `data <_static/labs/day2/day2_lab2_data.zip>`__

`answers <_static/labs/day2/day2_lab2_answers.R>`__


++++

12:30 - Lunch
-------------

++++

13:30 - Statistical Learning I (talk)
-------------------------------------
From linear regression to more complex models `(slides) <_static/talks/day2_3_statistical_learning_1.pdf>`__



14:00 - Statistical Learning I (lab)
------------------------------------
`handout <_static/labs/day2/day2_lab3.R>`__ , the data is the same as for the first lab of today

`answers <_static/labs/day2/day2_lab3_answers.R>`__

++++

15:00 - Tea
-----------

++++

15:15 - Statistical Learning II (talk)
--------------------------------------
Predictions with spatial data and cross-validation

Jiehua Chen


15:45 - Statistical Learning II (lab)
-------------------------------------
Validating and comparing models

`background <_static/labs/day2/Cross-validation.html>`__ and `handout <_static/labs/day2/day2_lab4.R>`__
