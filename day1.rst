Monday, August 15
=================


8:30 - Registration
-------------------


9:00 - Opening and Introductions
--------------------------------
Remarks by Jovin Lwehabura, CIAT and Sustainable Intensification Innovation Lab (SIIL)

Remarks by January Mafuru, director of the Selian Agricultural Research Institute (SARI), Arusha



9:30 - Introduction
-------------------
Introduction to workshop goals and approaches `(slides) <_static/talks/day1_1_course_introduction.pdf>`__


9:40 - Talk: R basics I
-----------------------
data input/output, aggregation, sub-setting `(slides) <_static/talks/day1_2_introR_1.pdf>`__



9:45 - Work: R basics I
-----------------------

`handout <_static/labs/day1/day1_lab1.R>`__ and `data <_static/labs/day1/day1_lab1_data.zip>`__

`answers <_static/labs/day1/day1_lab1_answers.R>`__

++++

10:30 - Group photo
-------------------

10:40 - Tea
-----------

++++

12:15 - Talk: Graphics with R
-----------------------------

++++

12:30 - Lunch
-------------

++++

13:30 - Talk: R basic II
------------------------
Branching, looping, apply, functions, coercion `(slides) <_static/talks/day1_4_plotting.pdf>`__



14:00 - Work: R basics II
-------------------------

`handout <_static/labs/day1/day1_lab3.R>`__ and `data <_static/labs/day1/day1_lab3_data.zip>`__

`answers <_static/labs/day1/day1_lab3_answers.R>`__

++++

15:00 - Tea
-----------

++++


15:15 - Spatial data I (talk)
-----------------------------
Vector data manipulation `(slides) <_static/talks/day1_5_spatial_1_vector.pdf>`__


Coordinate reference systems `(slides) <_static/talks/day1_6_spatial_2_crs.pdf>`__




15:45 - Spatial data I (lab)
----------------------------

`handout <_static/labs/day1/day1_lab4.R>`__ and `data <_static/labs/day1/day1_lab1_data.zip>`__ (same as for lab 1).

`answers <_static/labs/day1/day1_lab4_answers.R>`__
