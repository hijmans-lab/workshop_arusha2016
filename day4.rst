Thursday, August 18
===================


8:30 - Remote Sensing (talk I)
------------------------------
Introduction to remote sensing `(slides) <_static/talks/day4_1_remotesensing.pdf>`__

Ani Ghosh


9:00 - Remote Sensing (lab)
---------------------------
`handout <_static/labs/day4/day4_lab1_remote-sensing.pdf>`__ and `data <_static/labs/day4/day4_remotesensing_data.zip>`__




++++

10:30 - Coffee
--------------

12:00 - Remote Sensing (talk II)
--------------------------------
New developments in remote sensing (drones, Google Earth Engine) `(slides) <_static/talks/day4_2_remotesensing-advances.pdf>`__


++++

12:30 - Lunch
-------------

++++

13:30 - Spatial Sampling (talk)
-------------------------------
Jiehua Chen


13:45 - Spatial Sampling (lab)
-------------------------------
talk and handout `first <_static/talks/sampling/controlyield_analysis.html>`__ and `second <_static/talks/sampling/sampling_arusha.html>`__


++++

15:00 - Tea
-----------

15:15 - Leaflet and Shiny (talk)
--------------------------------
Interactive Maps, Charts and R `(slides) <_static/talks/day4_4_leaflet-shiny.html>`__ `(pdf slides) <_static/talks/day4_4_leaflet-shiny.pdf>`__

Alex Mandel


15:30 - Leaflet and Shiny (lab)
-------------------------------
`handout <_static/labs/day4/leaflet-shiny.zip>`__



16:45 - Crop growth simulation models (talk)
--------------------------------------------
Camila Bonilla `(slides) <_static/talks/day4_5_crop_simulation.pdf>`__


17:30 - Crop growth simulation models (lab)
-------------------------------------------
`handout <_static/labs/day4/Crop_Modelling.zip>`__
