---
title: "tutorial"
author: "Alex"
date: "August 8, 2016"
output: html_document
---

```{r setup, include=FALSE}
knitr::opts_chunk$set(echo = TRUE)
```


## Make a Map with Leaflet in R

*You will need to install and load the **leaflet** libary for this exercise.*

Start by making a basic **Leaflet** map in R. We'll use the default tiles **OpenStreetMap** and add a simple marker. Then we'll call the Leaflet map object and it will show up in the plot window.

> Leaflet is a little different from other plot methods. You start by making the Leaflet type object and then add items to that object. Finally you render that object at the end by calling it.

```{r basemap, fig.align='center'}
library(leaflet)
# Create a basic Leaflet Map
amap <- leaflet()
amap <- addTiles(amap) # The default is OpenStreetMap
amap <- addMarkers(amap, lng=174.768, lat=-36.852, popup="The birthplace of R")
amap
```

## Something more complicated
Now we can make the map more useful by changing to center location and the starting zoom level.

Exercise

1. Start the map the same way as before: `bmap <- leaflet()`
1. Center the map on the town of Arusha.
1. 

> To set the center we need a Latitude and Longitude value. We can use the `geocode` function from the **raster** package to lookup coordinates using Google.[^1]

Solution
```{r arusha}
library(raster)
# Recenter the Map on Arusha
bmap <- leaflet()
bmap <- addTiles(bmap) # The default is OpenStreetMap
if (!(exists("arusha"))){
  # Only need to geocode once per session
  library(dismo)
  arusha <- geocode("Arusha, TZ")  
}
bmap <- addMarkers(bmap, lng=arusha$longitude, lat=arusha$latitude, popup="Downtown Arusha")
# Change the default zoom
bmap <- setView(bmap, lng=arusha$longitude, lat=arusha$latitude, zoom = 12)
bmap
```

[^1] Use of the Google geocoder is subject to the terms and limits set by Google.

## Adding Vector Data to the Map

You are not limited to only using basemaps and markers. Any spatial data you can load in R with sp/rgdal can be used with a map.

Exercise: 

1. Load a polygon vector layer
1. Add the layer to the map with the `addPolygons` function.
1. Center the map on this new layer.

Solution:
```{r adding_vectors}
library(rgeos)
cmap <- leaflet()
cmap <- addTiles(cmap)
# Get country outline and level 2 admin
country <- getData(name="GADM", country="TZA", level=0)
regions <- getData(name="GADM", country="TZA", level=2)
# Find the center of the map
center <- coordinates(gPointOnSurface(country)[1])
# Add data to the map
cmap <- addPolygons(cmap, data=regions, stroke = TRUE, fillOpacity = 0.15, color = "Orange")
cmap <- setView(cmap, lng=center[1], lat=center[2], zoom = 6)
cmap
```

## Color vector data by values + Popup
```{r coloring}
dmap <- leaflet()
dmap <- addTiles(dmap)
factpal <- colorFactor(rainbow(10),regions$NAME_2)
dmap <- addPolygons(dmap, data=regions, stroke = TRUE, fillOpacity = 0.15, color =~factpal(NAME_2), popup=regions$NAME_2)
dmap <- setView(dmap, lng=center[1], lat=center[2], zoom = 6)
dmap
```

## Adding Raster Data to Map
```{r rasters}
emap <- leaflet()
elev <- getData(name="alt", country="TZA")
pal <- terrain.colors(20)
emap <- addRasterImage(emap, elev, colors = pal)
emap <- addPolygons(emap, data=regions, stroke = TRUE, weight=0.5, fillOpacity = 0 , color = "black", popup=regions$NAME_2)
emap
```

# Part 2 - Shiny

## Shiny, bring R to the web

In Rstudio, create a New File -> Shiny Web App...
A dialog will pop-up, name the folder to store the project and select Multiple File.

You now have 2 files ui.R and server.R
ui.R specifies how the user interface looks and interacts with the user.
server.R runs the data loading and processing.

To see how the demo app works, click on the > Run App button in the toolbar.

## Putting a map into Shiny


### ui.R

First you need to import the **leaflet** library at the top of *ui.R*

Now to replace the demo plot with a Leaflet map. In `mainPlot` section remove the `plotOutput` command and replace it with ``leafletOutput` command.

```{r ui, eval=FALSE}
mainPanel(
      leafletOutput("mymap")
    )
```

### server.R

In the server.R file we will now define "mymap". Also import the **leaflet** library in *server.R*

```{r server, eval=FALSE}
output$mymap <- renderLeaflet({
    #Put your code here.
  })
```

Now copy one of your working leaflet maps from the previous section. It doesn't matter which one, just make sure to include all code needed to make the map

Once you've added the code, save the files and try the Run App button. You should get a pop-up window with your map.

For more information see the Leaflet Shiny help page <todo link>.

## Making map react to user input

To add more user interaction lets add functionality to change the colors.



## What's Next

Shiny has a lot of other things it can do. Things to keep in mind.
If you want to share your project publicly you can use the shinyapps.io service.
If you need it to stay private you need to either share your code and data, or run your own Shiny Server (link). If you run your own server keep in mind that the more complicated the App, the fewer people can use it at the same time without needing more servers. Shiny by default only works well for 3-5 simultaneous users.


