Wednesday, August 17
====================

8:30 - Spatial Interpolation (talk)
-----------------------------------
`(slides) <_static/talks/day3_1_interpolation.pdf>`__


9:00 - Spatial Interpolation (lab)
-----------------------------------
Work through `this web-page <http://rspatial.org/analysis/rst/4-interpolation.html>`__ and answer the questions.


++++

10:30 - Coffee
--------------

++++

10:45 - Spatial Interpolation (lab 2)
-------------------------------------

Make a high resolution soils map (`handout <_static/labs/day3/day3_lab2.R>`__ and `data <_static/labs/day3/day3_data_TZA.zip>`__)


11:30 - New informatics tools (talk)
------------------------------------
William Wu `(slides) <http://qed.ai/QED-SPEED-2016-08.pdf>`__



++++

12:30 - Lunch
-------------

++++

13:30 - Spatial Prediction (talk)
---------------------------------
Spatial distribution models. Random Forest `(slides) <_static/talks/day3_3_spatial_dist_models.pdf>`__


14:00 - Spatial Prediction (lab)
--------------------------------

Work through `this web-page <http://rspatial.org/analysis/rst/5-global_regression.html>`__ and answer the questions.


++++

15:00 - Tea
-----------

++++

15:15 - Spatial Prediction (lab 2)
----------------------------------

Predict future agricultural expansion in Tanzania (`handout <_static/labs/day3/day3_lab4.R>`__ and `data <_static/labs/day3/day3_data_TZA.zip>`__)
